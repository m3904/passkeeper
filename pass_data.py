# -*- coding: utf-8 -*-
import csv
from pathlib import Path
from typing import Dict, List, IO
from datetime import datetime


class FormatError(Exception):
    pass


class CsvPassData:
    field_names = ('name', 'user', 'password', 'url', 'notes', 'date')
    immediate_flush = True

    @classmethod
    def open(cls, name: str, codec_name: str = "utf-8"):
        path = Path(name)
        if not path.exists():
            path.parent.mkdir(exist_ok=True)
            with path.open("w", encoding=codec_name) as f:
                print(*cls.field_names, sep=',', file=f)
        return path.open("r+", encoding=codec_name)

    def __init__(self, stream: IO):
        self.stream = stream
        reader = csv.DictReader(stream)
        if tuple(reader.fieldnames) != CsvPassData.field_names:
            raise FormatError()
        self.rows = dict()
        for row in reader:
            self.rows[row['name']] = row

    @classmethod
    def fill_keys(cls, data: Dict[str, str]):
        missed = set(cls.field_names).difference(data.keys())
        data.update(dict.fromkeys(missed, ''))

    @classmethod
    def check_keys(cls, data: Dict[str, str]):
        extra = set(data.keys()).difference(cls.field_names)
        if extra:
            raise FormatError()

    def add(self, data: Dict[str, str]):
        self.check_keys(data)
        data['date'] = datetime.now().isoformat()
        self.rows[data['name']] = data
        if CsvPassData.immediate_flush:
            self.save()

    def delete(self, name: str):
        del self.rows[name]
        if CsvPassData.immediate_flush:
            self.save()

    def get_by_name(self, name: str) -> Dict[str]:
        return self.rows.get(name, dict())

    def get_by_url(self, url: str) -> List[Dict[str]]:
        return list(filter(lambda x: x['url'] == url, self.rows.values()))

    def save(self):
        self.stream.seek(0)
        writer = csv.DictWriter(self.stream, fieldnames=self.field_names)
        for row in self.rows.values():
            writer.writerow(row)
